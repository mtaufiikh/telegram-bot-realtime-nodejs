const telegramBot = require('node-telegram-bot-api');
const token = '940522743:AAEiQYW_GVn8mpg5v8KBvaA-qeQjTTEtDYg';
const bot = new telegramBot(token, {polling:true});

bot.on('message', (msg) => {
    const chatId = msg.chat.id;
    const chatName = msg.chat.first_name;
    if(msg.text == "/start"){
        bot.sendMessage(chatId, "Ayo main suit Jepang dengan ku. Klik --> /GameOn untuk main game");
    }
});

bot.onText(/\/GameOn/, function onLoveText(msg) {
    const opts = {
      reply_to_message_id: msg.message_id,
      reply_markup: JSON.stringify({
        keyboard: [
          ['👊 Batu'],
          ['✌️ Gunting'],
          ['✋ Kertas']
        ]
      })
    };
    bot.sendMessage(msg.chat.id, 'Pilih avatar mu..', opts);
});


bot.on('message', (msg) => {
    const chatId = msg.chat.id;
    const chatName = msg.chat.first_name;
    const chatText = msg.text;
    let valueBot = Math.random();

    switch (chatText) {
        case "👊 Batu":
                if(valueBot > 0 && valueBot < 0.33){
                    bot.sendMessage(chatId, "Bot : Batu 👊 Vs 👊 Batu >> Yah Seri");
                }
                if(valueBot >= 0.33 && valueBot < 0.66){
                    bot.sendMessage(chatId, "Bot : Gunting ✌️ Vs 👊 Batu >> Hore Kamu Menang");
                }
                if(valueBot >= 0.66 && valueBot < 1){
                    bot.sendMessage(chatId, "Bot : Kertas ✋ Vs 👊 Batu >> Modaro, Kalah Koe");
                }
            break;
        case "✌️ Gunting":
                if(valueBot > 0 && valueBot < 0.33){
                    bot.sendMessage(chatId, "Bot : Batu 👊 Vs ✌️ Gunting >> Modaro, Kalah Koe");
                }
                if(valueBot >= 0.33 && valueBot < 0.66){
                    bot.sendMessage(chatId, "Bot : Gunting ✌️ Vs ✌️ Gunting >> Yah Seri");
                }
                if(valueBot >= 0.66 && valueBot < 1){
                    bot.sendMessage(chatId, "Bot : Kertas ✋ Vs ✌️ Gunting >> Hore Kamu Menang");
                }
            break;
        case "✋ Kertas":
                if(valueBot > 0 && valueBot < 0.33){
                    bot.sendMessage(chatId, "Bot : Batu 👊 Vs ✋ Kertas >> Hore Kamu Menang");
                }
                if(valueBot >= 0.33 && valueBot < 0.66){
                    bot.sendMessage(chatId, "Bot : Gunting ✌️ Vs ✋ Kertas >> Modaro, Kalah Koe");
                }
                if(valueBot >= 0.66 && valueBot < 1){
                    bot.sendMessage(chatId, "Bot : Kertas ✋ Vs ✋ Kertas >> Yah Seri");
                }
            break;
        default:
                if(chatText != "/GameOn")
                    bot.sendMessage(chatId, "/GameOn untuk Main");
            break;
    }

});

bot.onText(/\/editable/, function onEditableText(msg) {
    const opts = {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Edit Text',
              // we shall check for this value when we listen
              // for "callback_query"
              callback_data: 'edit'
            }
          ]
        ]
      }
    };
    bot.sendMessage(msg.from.id, 'Original Text', opts);
  });
  
  
  // Handle callback queries
  bot.on('callback_query', function onCallbackQuery(callbackQuery) {
    const action = callbackQuery.data;
    const msg = callbackQuery.message;
    const opts = {
      chat_id: msg.chat.id,
      message_id: msg.message_id,
    };
    let text;
  
    if (action === 'edit') {
      text = 'Edited Text';
    }
  
    bot.editMessageText(text, opts);
  });